/**
 * Import module express (line 1) - Your code here
 * Instance express (line 2) - Your code here
 */
const express = require("express");
const app = express();
// Middleware
const logger = (req, res, next) => {
    let today = new Date();
    let dd = today.getDate();
    let mm = today.getMonth() + 1;
    let yyyy = today.getFullYear();
    let hh = today.getHours();
    let nn = today.getMinutes();
    let ss = today.getSeconds();
  
    if (dd < 10) dd = "0" + dd;
    if (mm < 10) mm = "0" + mm;
    if (hh < 10) hh = "0" + hh;
    if (nn < 10) mm = "0" + nn;
    if (ss < 10) ss = "0" + ss;
  
    let date = dd + "/" + mm + "/" + yyyy;
    let time = hh + ":" + nn + ":" + ss;
  
    console.log("Request GET/ at " + date + "" + time);
    // Your code here (gunakan parameter pada middleware)
    
  next()
  };
  
  /**
   * Using middleware
   * Endpoint untuk landing page => Using Middleware (header)
   * Listen server running at port 3000
   * 
   */

  app.use(logger)

  app.get('/', (req, res) => {
    const header = `
    <h1>Using Middleware</h1>
    `;
    res.send(header);
});

app.listen(3000, () => {
  console.log("Server Running at Port 3000")
})