/**
 * Import module express (line 1)
 * Instance express (line 2)
 */
const express = require('express')
const app = express()


 app.get("/", (req, res) => {
    const body = `
      <h2>Landing Page</h2>
      <a href="http://localhost:3000/catalog">Catalog</a> |
      <a href="http://localhost:3000/contact">Contact Us</a> |
      <a href="http://localhost:3000/page-not-found">Another Route</a>
      `;
  
    res.send(body);
  });
  
  /**
   * Route untuk landing page => Landing Page (header)
   * Route untuk catalog page => Catalog Page (header)
   * Route untuk page not found => 404: Page Not Found (header)
   *
   * Listen server running at port 3000
   */

  app.get("/catalog", (req, res) => {
      const catalog = `
      <h1>Catalog Page </h1>
      `;
      res.send(catalog);
  });
  
  app.get("/contact", (req, res) => {
    const contact = `
    <h1>Catalog Page </h1>
    `;
    res.send(contact);
})

app.get("/page-not-found", (req, res) => {
    const page = `
    <h1>page-not-found</h1>
    `;
    res.send(page);
})

app.listen(3000, () => console.log("Server Running at port 3000"))