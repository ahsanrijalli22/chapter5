const https = require("https");
const fs = require("fs");

const options = {
    key: fs.readFileSync("./key.pem"),
    cert: fs.readFileSync("./cert.pem"),

};
https 
.createServer(options, function (req, res){
    res.end("Hello world");
})
.listen(3000);

// openssl genrsa 1024 > key.pem
// openssl req -x509 -new -key key.pem > cert.pem

//curl https://localhost:3000 -k
