const http = require("http");
const fs = require("fs");

function onRequest(res, res){
    res.writeHead(200, {"Content-Type": "text/html" });
    fs.readFile("./index.html", null, (error, data) => {
        if (error) {
            res.writeHead(404);
            res.write("file not Found");
        } else {

        res.write(data);
        }
        res.end();
    })
}

http.createServer(onRequest).listen(8000)