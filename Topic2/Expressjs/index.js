// Routing

const express = require("express");
const app = express();


// Setiap request GET ke http://localhost:3000/ akan diarahkan ke handler ini
app.get("/", (req, res) => {
    res.end("Hello World")
})

// Setiap request GET ke http://localhost:3000/product akan diarahkan ke handler ini
app.get("/product", (req, res) => {
    res.json(["Pen", "Pensil", "Book"])
})

// Setiap request GET ke http://localhost:3000/orders akan diarahkan ke handler ini
app.get("/orders", (req, res) => {
    res.json([
        {
            id: 1,
            paid: false,
            user_id: 1,
        },
        {
            id: 2,
            paid: true,
            user_id: 1,
        }
    ])
})

app.listen(3000, () => console.log("Server Running at port 3000"))
